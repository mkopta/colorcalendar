import datetime
from calendar import *
from calendar import formatstring


class Colors:
    black    = '\033[30m'
    blue     = '\033[34m'
    green    = '\033[32m'
    cyan     = '\033[36m'
    red      = '\033[31m'
    purple   = '\033[35m'
    brown    = '\033[33m'
    gray     = '\033[37m'
    yellow   = '\033[33m'
    white    = '\033[37m'

    reset       = '\033[0m'


class ColorCalendar(TextCalendar):
    def __init__(self):
        super().__init__()
        self.hldays = []

    def formatday(self, year, month, day, weekday, width):
        s = super().formatday(day, weekday, width)
        if day != 0:
            for hlday_and_color in self.hldays:
                if (year, month, day) == hlday_and_color[:3]:
                    s = hlday_and_color[-1] + s + Colors.reset
        return s

    def formatweek(self, year, month, theweek, width):
        return ' '.join(self.formatday(
            year, month, d, wd, width) for (d, wd) in theweek)

    def formatmonth(self, theyear, themonth, w=0, l=0):
        w = max(2, w)
        l = max(1, l)
        s = self.formatmonthname(theyear, themonth, 7 * (w + 1) - 1)
        s = s.rstrip()
        s += '\n' * l
        s += self.formatweekheader(w).rstrip()
        s += '\n' * l
        for week in self.monthdays2calendar(theyear, themonth):
            s += self.formatweek(theyear, themonth, week, w).rstrip()
            s += '\n' * l
        return s

    def formatyear(self, theyear, w=2, l=1, c=6, m=3):
        w = max(2, w)
        l = max(1, l)
        c = max(2, c)
        colwidth = (w + 1) * 7 - 1
        v = []
        a = v.append
        a(repr(theyear).center(colwidth*m+c*(m-1)).rstrip())
        a('\n'*l)
        header = self.formatweekheader(w)
        for (i, row) in enumerate(self.yeardays2calendar(theyear, m)):
            # months in this row
            months = range(m*i+1, min(m*(i+1)+1, 13))
            a('\n'*l)
            names = (self.formatmonthname(theyear, k, colwidth, False)
                     for k in months)
            a(formatstring(names, colwidth, c).rstrip())
            a('\n'*l)
            headers = (header for k in months)
            a(formatstring(headers, colwidth, c).rstrip())
            a('\n'*l)
            # max number of weeks for this row
            height = max(len(cal) for cal in row)
            for j in range(height):
                weeks = []
                i = 0
                for cal in row:
                    if j >= len(cal):
                        weeks.append('')
                    else:
                        weeks.append(
                            self.formatweek(
                                theyear, list(months)[i], cal[j], w))
                    i += 1
                    i = i % len(months)
                a(formatstring(weeks, colwidth, c).rstrip())
                a('\n' * l)
        return ''.join(v)


cc = ColorCalendar()
cc.hldays.extend([
    (2022, 2, 14, Colors.red),
    (2022, 3, 3, Colors.green),
    (2022, 3, 22, Colors.green),
    (2022, 6, 30, '\033[1;47;30m'),
    (2022, 12, 19, Colors.green),
    (2022, 12, 24, Colors.green)])
for n in range(16, 21):
    cc.hldays.append((2022, 3, n, Colors.yellow))
today = datetime.date.today()
cc.hldays.append((today.year, today.month, today.day, Colors.blue))
cc.pryear(2022)
